package Server;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import DataBase.CheckdataBase;

import javax.swing.JButton;
import javax.swing.JPasswordField;

public class FormDangKy extends JFrame implements ActionListener{

	private JFrame frame;
	private JTextField txtTaiKhoan;
	private JTextField txtPort;
	private JButton btDangKy;
	private JPasswordField txtMatKhau;
	private JPasswordField txtXacNhan;

	/**
	 * Launch the application.
	 */


	/**
	 * Create the application.
	 */
	public FormDangKy() {
		super();
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblTiKhon = new JLabel("Tài Khoản");
		lblTiKhon.setBounds(79, 43, 63, 14);
		frame.getContentPane().add(lblTiKhon);
		
		JLabel lblMtKhu = new JLabel("Mật Khẩu");
		lblMtKhu.setBounds(79, 77, 63, 14);
		frame.getContentPane().add(lblMtKhu);
		
		JLabel lblXcNhn = new JLabel("Xác nhận");
		lblXcNhn.setBounds(79, 116, 63, 14);
		frame.getContentPane().add(lblXcNhn);
		
		txtTaiKhoan = new JTextField();
		txtTaiKhoan.setBounds(180, 40, 167, 20);
		frame.getContentPane().add(txtTaiKhoan);
		txtTaiKhoan.setColumns(10);
		
		JLabel lblCng = new JLabel("Cổng");
		lblCng.setBounds(79, 157, 63, 14);
		frame.getContentPane().add(lblCng);
		
		txtPort = new JTextField();
		txtPort.setBounds(180, 154, 167, 20);
		frame.getContentPane().add(txtPort);
		txtPort.setColumns(10);
		
		 btDangKy = new JButton("Đăng Ký");
		btDangKy.setBounds(221, 199, 89, 23);
		frame.getContentPane().add(btDangKy);
		
		txtMatKhau = new JPasswordField();
		txtMatKhau.setBounds(180, 74, 167, 20);
		frame.getContentPane().add(txtMatKhau);
		
		txtXacNhan = new JPasswordField();
		txtXacNhan.setBounds(180, 113, 167, 20);
		frame.getContentPane().add(txtXacNhan);
		btDangKy.addActionListener(this);
		frame.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == btDangKy){
			String user = txtTaiKhoan.getText();
			String password = txtMatKhau.getText();
			String xacnhan = txtXacNhan.getText();
			String port = txtPort.getText();
			if (user.equals("")  || port.equals("")){
				JOptionPane.showMessageDialog(this, "Nhập lại thông tin");
			}
			else if (!password.equals(xacnhan)) {
				JOptionPane.showMessageDialog(this, "Mật khẩu xác nhận không đúng");
			}
			else if (new CheckdataBase().checkTaiKhoan(user)){
				JOptionPane.showMessageDialog(this, "Tài khoản đã tồn tại");
			}
			else if (new CheckdataBase().checkPort(Integer.parseInt(port),user,password)){
				JOptionPane.showMessageDialog(this, "Cổng đã tồn tại , nhập cổng khác");
				
			}
			else {
				new FormDangNhap();
				this.setVisible(false);
			}
		}
		
	}
}
