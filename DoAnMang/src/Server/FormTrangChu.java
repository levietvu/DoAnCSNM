package Server;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import DataBase.Online;

import javax.swing.JButton;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JList;
import javax.swing.JLabel;
import javax.swing.JScrollBar;
import javax.swing.JCheckBox;

public class FormTrangChu extends JFrame implements ActionListener{

	private JFrame frame;
	private String tittle;
	private JTextField textField;
	private JTextField textField_1;
	private JButton btLogout;
	public String getTittle() {
		return tittle;
	}

	public void setTittle(String tittle) {
		this.tittle = tittle;
	}

	/**
	 * Launch the application.
	 */


	/**
	 * Create the application.
	 */
	public FormTrangChu(String tittle) {
		super();
		this.tittle = tittle;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame("Hello "+ tittle);
		frame.setBounds(100, 100, 490, 319);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		textField = new JTextField();
		textField.setBounds(44, 212, 246, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		JButton btnGi = new JButton("Send");
		btnGi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnGi.setBounds(332, 211, 76, 23);
		frame.getContentPane().add(btnGi);
		
		JButton btnSendfile = new JButton("SendFile");
		btnSendfile.setBounds(31, 11, 89, 23);
		frame.getContentPane().add(btnSendfile);
		
		JButton btnConnect = new JButton("Connect");
		btnConnect.setBounds(303, 177, 73, 23);
		frame.getContentPane().add(btnConnect);
		
		JTextArea textArea = new JTextArea();
		textArea.setEnabled(false);
		textArea.setBackground(Color.LIGHT_GRAY);
		textArea.setLineWrap(true);
		textArea.setColumns(1);
		textArea.setBounds(44, 64, 246, 134);
		frame.getContentPane().add(textArea);
		
		JButton btnNewButton = new JButton("Refresh");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnNewButton.setBounds(388, 177, 76, 23);
		frame.getContentPane().add(btnNewButton);
		
		textField_1 = new JTextField();
		textField_1.setEnabled(false);
		textField_1.setBounds(134, 12, 156, 20);
		frame.getContentPane().add(textField_1);
		textField_1.setColumns(10);
		
		JLabel lblTn = new JLabel("Name");
		lblTn.setBounds(332, 36, 46, 14);
		frame.getContentPane().add(lblTn);
		
		JLabel lblPort = new JLabel("Port");
		lblPort.setBounds(388, 36, 46, 14);
		frame.getContentPane().add(lblPort);
		
		JScrollBar scrollBar = new JScrollBar();
		scrollBar.setBounds(44, 64, 246, 134);
		frame.getContentPane().add(scrollBar);
		
		btLogout = new JButton("LogOut");
		btLogout.setBounds(172, 247, 89, 23);
		btLogout.addActionListener(this);
		frame.getContentPane().add(btLogout);
		frame.setVisible(true);
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if (e.getSource() == btLogout){
			new Online().xoaOnline(tittle);
			new FormDangNhap();
			this.setVisible(false);
		}
		
	}

	
}
