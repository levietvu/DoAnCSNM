package Server;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JFrame;
import javax.swing.JTextField;

import DataBase.CheckdataBase;
import DataBase.Online;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.jws.soap.SOAPBinding.Use;
import javax.swing.JButton;
import javax.swing.JPasswordField;

public class FormDangNhap extends JFrame implements ActionListener {

	private JFrame frame;
	private JTextField txtLogin;
	private JLabel lblTiKhon;
	private JLabel lblMtKhu;
	private JButton btLogin;
	private JButton btRegister;
	private JPasswordField passwordField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		new FormDangNhap();
//		EventQueue.invokeLater(new Runnable() {
//			public void run() {
//				try {
//					FormDangNhap window = new FormDangNhap();
//					window.frame.setVisible(true);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//		});
	}

	/**
	 * Create the application.
	 */
	public FormDangNhap() {
		super();
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		txtLogin = new JTextField();
		txtLogin.setBounds(170, 72, 130, 20);
		frame.getContentPane().add(txtLogin);
		txtLogin.setColumns(10);
		
		lblTiKhon = new JLabel("Tài Khoản");
		lblTiKhon.setBounds(73, 74, 64, 17);
		frame.getContentPane().add(lblTiKhon);
		
		lblMtKhu = new JLabel("Mật Khẩu");
		lblMtKhu.setBounds(76, 129, 61, 14);
		frame.getContentPane().add(lblMtKhu);
		
		 btLogin = new JButton("Đăng Nhập");
		btLogin.setBounds(116, 179, 96, 23);
		frame.getContentPane().add(btLogin);
		
		btRegister = new JButton("Đăng ký");
		btRegister.setBounds(238, 179, 89, 23);
		frame.getContentPane().add(btRegister);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(170, 126, 130, 20);
		frame.getContentPane().add(passwordField);
		btLogin.addActionListener(this);
		btRegister.addActionListener(this);
		frame.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == btLogin){
			String user = txtLogin.getText();
			String password = passwordField.getText();
			if (user.equals("") || password.equals("")){
				JOptionPane.showMessageDialog(this, "Yêu cầu nhập đầy đủ  thông tin");
			}
			else {
				try {
					if (new CheckdataBase().checkAccount(user,password)){
						new Online().themOnline(user);
						new FormTrangChu(user);
						frame.setVisible(false);
					}
					else {
						JOptionPane.showMessageDialog(this, "Tài khoản hoặc mật khẩu chưa chính xác");
					}
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			
		}
		if (e.getSource() == btRegister){
			
			new FormDangKy();
			frame.setVisible(false);
		}
		
	}
}
