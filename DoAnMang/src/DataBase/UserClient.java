package DataBase;

public class UserClient {
	private String taiknhoan;
	private String matkhau;
	private int port;
	public UserClient(String taikhoan, String matkhau, int port) {
		// TODO Auto-generated constructor stub
		this.setTaiknhoan(taikhoan);
		this.setMatkhau(matkhau);
		this.setPort(port);
	}
	public String getTaiknhoan() {
		return taiknhoan;
	}
	public void setTaiknhoan(String taiknhoan) {
		this.taiknhoan = taiknhoan;
	}
	public String getMatkhau() {
		return matkhau;
	}
	public void setMatkhau(String matkhau) {
		this.matkhau = matkhau;
	}
	public int getPort() {
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}
}
